#!/usr/bin/env bash

# set -x;

RSYNC='rsync -ah --info=progress2,stats --no-i-r'
# RSYNC='echo #rsync -ah --info=progress2 --no-i-r'

# check rsync version
CURVERSION=$(rsync --version | sed -n '1s/^rsync *version \([0-9.]*\).*$/\1/p')
REQVERSION="3.1.0"
if ! [ "$(printf '%s\n' "$REQVERSION" "$CURVERSION" | sort -V | head -n1)" = "$REQVERSION" ]; then
    echo "Less than $REQVERSION"
    echo "Need to install newer version of rsync"
    echo "  brew install rsync"
    exit 1
fi

_realpath() {
    [[ $1 = /* ]] && echo "$1" || echo "$PWD/${1#./}"
}

gen_bar() {
  local str=$1
  local num=$2
  local v="$(printf "%-${num}s" "$str")"
  echo "${v// /$str}"
}

progress_bar()
{
  local p=$(echo "$1" | tr -d '%')

  local tcols="$(tput cols)"
  local ncols=$(($tcols-${#p}-3))

  local strdone="$(gen_bar "#" $ncols)"
  local strtodo="$(gen_bar ' ' $ncols)"

  local lendone=$(($p * ${#strdone} / 100))
  let lentodo=${#strdone}-$lendone

  printf '%s\r' "${strdone:0:$lendone}${strtodo:0:$lentodo} $p%"
  # gecho -ne "[${strdone:0:$lendone}${strtodo:0:$lentodo}] $p%\r"

}

cp_echo()
{
  # echo "cp $( basename "$1" ) → $( dirname "$2" )..."
  echo "cp $( basename "$1" ) to $2..."
  # echo "cp $( basename "$1" )..."
}

foo()
{
    while read data; do
        progress_bar "$data"
    done
}

cpr()
{

    local SRC=("${@:1:$#-1}")
    local DST="${@: -1}"

    local i=1

    # copy
    for src in "${SRC[@]}"
    do
        rm -Rf $FILE_LIST
        # convert to complete path
        src="$(_realpath "$src")"

        # cp_echo "$src" "$DST"

        $RSYNC "$src" "$DST" | stdbuf -oL awk 'BEGIN { RS="\r" } /%/ { print $2 }' | foo & wait
        echo

    done

}

# eof
