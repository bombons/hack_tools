## Hackintosh Guide Tools

This project contains various scripts and tools common to my hackintosh guide projects.

Unless you're planning to create you own guide with automated scripts, this project is not directly useful to you.  Refer to my other projects to see how this project is used with them.

### Credits

RehabMan: most of the code here

zlib: RevoGirl

tag: https://github.com/jdberry/tag
