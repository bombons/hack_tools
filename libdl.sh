#!/bin/bash

# set -x

curl_options="--retry 5 --location --progress-bar"
curl_options_silent="--retry 5 --location --silent"
tmp=/tmp/com.bombons.download.txt

# download from bitbucket downloads
function download_bitbucket
# $1 is account name on bitbucket
# $2 is subdir on account bitbucket
# $3 is prefix of zip file name
# $4 is output file name (optional, otherwise uses remote name)
{
    echo "downloading $3:"
    curl $curl_options_silent --output $tmp https://bitbucket.org/$1/$2/downloads/
    if [ "$4" == "" ]; then
        local scrape="$(grep -o -m 1 "/$1/$2/downloads/$3.*\.zip" $tmp | perl -ne 'print $1 if /(.*)\"/')"
    else
        local scrape="$(grep -o -m 1 "/$1/$2/downloads/$3--$4.*\.zip" $tmp | perl -ne 'print $1 if /(.*)\"/')"
    fi
    local url=https://bitbucket.org$scrape
    echo $url
    curl $curl_options --remote-name "$url"
    echo
}

# download typical release from RehabMan bitbucket downloads
function download_rehabman
# $1 is subdir on rehabman bitbucket
# $2 is prefix of zip file name
# $3 is output file name (optional, otherwise uses remote name)
{
    download_bitbucket "RehabMan" "$1" "$2" "$3"
}

# download typical release from RehabMan bitbucket downloads
function download_bombons
# $1 is subdir on rehabman bitbucket
# $2 is prefix of zip file name
# $3 is output file name (optional, otherwise uses remote name)
{
    download_bitbucket "bombons" "$1" "$2" "$3"
}

# download latest release from github (perhaps others)
function download_latest_notbitbucket
# $1 is main URL
# $2 is URL of release page
# $3 is partial file name to look for
# $4 is output file name (not optional)
{
    echo "downloading $4:"
    curl $curl_options_silent --output $tmp "$2"
    local scrape="$(grep -o -m 1 "/.*$3.*\.zip" $tmp)"
    local url=$1$scrape
    echo $url
    curl $curl_options --output "$4" "$url"
    echo
}

# download from acidanthera project on github
function download_acidanthera
# $1 is name of acidanthera project on github
# $2 is basename of output zip
{
    download_latest_notbitbucket "https://github.com" "https://github.com/acidanthera/$1/releases" "RELEASE" "$2.zip"
}

# https://github.com/acidanthera/IOJones/releases/latest
# download_github "acidanthera" "IOJones"
function download_github
# $1 is autor page
# $2 is project page
# $4 is output file name (not optional)
{
    echo "downloading $2:"
    curl $curl_options_silent --output $tmp "https://github.com/$1/$2/releases/latest"

    if [[ -z "$3" ]]; then
        local scrape="$(grep -o -m 1 "/.*$2.*\.zip" $tmp)"
        local extension="${scrape##*.}"
        local out="$1-$2.$extension"
    else
        local scrape="$(grep -o -m 1 "$3" $tmp)"
    fi

    local url=https://github.com$scrape
    echo $url

    if [[ -z "$3" ]]; then
        curl $curl_options --output "$out" "$url"
    else
        curl $curl_options --remote-name "$url"
    fi

    echo
}

download_direct()
# $1 url
# $2 output
# $3 name
{
    echo "downloading $2:"
    local url=$1
    echo $url
    if [[ -z "$3" ]]; then
        curl $curl_options --remote-name "$url"
    else
        curl $curl_options --output "$3" "$url"
    fi
    echo
}
#EOF
